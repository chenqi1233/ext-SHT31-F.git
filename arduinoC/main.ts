//% color="#ff9f06" iconWidth=50 iconHeight=40
namespace DFRobot_SHT31{

    //% block="DFRobot_SHT31 Init [ADDRESS] " blockType="command"
    //% ADDRESS.shadow="dropdown" ADDRESS.options="ADDRESS"  
    export function DFRobot_SHT31Init(parameter: any, block: any) {
        let address=parameter.ADDRESS.code;
         
     

        Generator.addInclude("DFRobot_SHT31Init", "#include <DFRobot_SHT3x.h>");
        
        let address=parameter.ADDRESS.code;
        Generator.addObject("DFRobot_SHT31Init","DFRobot_SHT3x",`sht3x(&Wire,${address},4);`);
  
        Generator.addSetup(`DFRobot_SHT31Init`, `while (sht3x.begin() != 0);`);
        
        
    }
   
    //% block="DFRobot_SHT31  DATA[DATA] " blockType="reporter" 
    //% DATA.shadow="dropdown" DATA.options="DATA"
    export function DFRobot_CCS811GetData(parameter: any, block: any) { 
        let data=parameter.DATA.code;
        
        Generator.addCode( [`sht3x.${data}()`,Generator.ORDER_UNARY_POSTFIX]);
   
   }
  /* //% block="DFRobot_SHT31  MODE " blockType="command" 
    
    export function DFRobot_CCS811SetMode(parameter: any, block: any) { 
         
        
        Generator.addCode("sht3x.stopPeriodicMode();");
   
   }*/
   
}