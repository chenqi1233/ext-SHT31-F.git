# Gravity SHT31-F数字温湿度传感器 
一款高精度、响应快、有防尘功能的传感器，可测量温度、湿度，适用于天气站、动物、植物养殖等应用

![](./arduinoC/_images/featured.png)
 
# 产品简介

- 产品链接：[Gravity SHT31-F数字温湿度传感器](https://www.dfrobot.com.cn/goods-2689.html)  

- 介绍： 本扩展库为DFRobot Gravity SHT31-F数字温湿度传感器（SEN0334）设计，支持 Mind+ 导入库，要求 Mind+ 软件版本为 1.6.2 及以上。

# 积木


![](./arduinoC/_images/blocks.png)

# 程序实例

 

![](./arduinoC/_images/example.png)

# 支持列表

|主板型号|实时模式|ArduinoC|MicroPython|备注|
|-----|-----|:-----:|-----|-----|
|uno||√|||
|micro:bit||√|||
|mpython||√|||
|arduinonano||√|||
|leonardo||√|||
|mega2560||√|||


# 更新日志

V0.0.1 基础功能完成

